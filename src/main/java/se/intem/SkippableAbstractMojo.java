package se.intem;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

public abstract class SkippableAbstractMojo extends AbstractMojo {

    @Parameter(defaultValue = "false", property = "k8s.skip")
    private boolean skip;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        if (skip) {
            getLog().info("Skipping plugin execution as per configuration");
            return;
        }

        executeMojo();
    }

    public abstract void executeMojo() throws MojoExecutionException, MojoFailureException;
}
