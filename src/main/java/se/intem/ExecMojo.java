package se.intem;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "exec")
public class ExecMojo extends SkippableAbstractMojo {

    @Parameter
    private List<String> commands = Collections.emptyList();

    // TODO Allow separate working directory per command in list?
    @Parameter(defaultValue = "${project.basedir}")
    private File workingDirectory;

    public void executeMojo() throws MojoExecutionException {

        getLog().debug("Executing commands " + commands + " in " + workingDirectory);

        for (String command : commands) {
            int exitValue = executeCommand(command);
            if (exitValue != 0) {
                throw new MojoExecutionException("Exit value " +
                    exitValue + " for command '" + command + "'");
            }
        }

    }

    private int executeCommand(String commandline) throws MojoExecutionException {
        try {
            List<String> command = tokenize(commandline);
            Process process = new ProcessBuilder(command)
                .directory(workingDirectory)
                .start();

            String stdout;
            try (InputStreamReader reader = new InputStreamReader(process.getInputStream(),
                Charsets.UTF_8)) {
                stdout = CharStreams.toString(reader);
            }

            String stderr;
            try (InputStreamReader reader = new InputStreamReader(process.getErrorStream(),
                Charsets.UTF_8)) {
                stderr = CharStreams.toString(reader);
            }

            boolean result = process
                .waitFor(10, TimeUnit.SECONDS);
            int exitValue = process.exitValue();

            if (result && exitValue == 0) {
                getLog().info("Executed '" + commandline + "':" + System.lineSeparator() + stdout);
            } else {
                getLog().error("Executed '" + commandline + "' with error:" + System.lineSeparator() + stderr);
            }

            return exitValue;

        } catch (InterruptedException | IOException e) {
            throw new MojoExecutionException("Failed to execute '" + commandline + "'", e);
        }
    }

    /* https://stackoverflow.com/a/7804472/19392 */
    private static List<String> tokenize(String commandline) {
        List<String> list = new ArrayList<>();
        Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(commandline);
        while (m.find()) {
            list.add(m.group(1));
        }

        return list;
    }

}
